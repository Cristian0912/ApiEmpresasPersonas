from django.conf.urls import url, include

urlpatterns = [
    url(r'^persona/', include('api.v1.persona_component.urls', namespace='personas')),
]
