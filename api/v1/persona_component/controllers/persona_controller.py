from api.v1.persona_component.managers.persona_manager import ObtenerPersonas, EliminarPersona, ObtenerUnaPersona, \
    GuardarPersona, UpdatePersona
from api.v1.persona_component.serializers.persona_serializer import PersonaSerializer


class PersonaController():

    @staticmethod
    def getPersonas():
        return ObtenerPersonas()

    @staticmethod
    def getPersona(id):
        return ObtenerUnaPersona(id= id)

    @staticmethod
    def savePersona(data):
        serializer = PersonaSerializer(data= data)
        if serializer.is_valid():
            GuardarPersona(serializer)
            return serializer
        return serializer

    @staticmethod
    def delpersona(id):
        return EliminarPersona(id)

    @staticmethod
    def updatePersona(id, data):
        return UpdatePersona(id, data)



