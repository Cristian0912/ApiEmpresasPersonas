from api.v1.persona_component.serializers.persona_serializer import PersonaSerializer
from apps.persona.models import PersonaModel
from django.http import Http404


"""Funcion para obtener a todas las personas"""

def ObtenerPersonas():
    personas = PersonaModel.objects.all()
    serializer = PersonaSerializer(personas, many=True)
    print(serializer.data)
    return serializer.data

"""Obtiene a una persona por su ID"""
def ObtenerUnaPersona(id):
    try:
        persona = PersonaModel.objects.get(pk=id)
        serializer = PersonaSerializer(persona, many=False)
        return serializer.data
    except PersonaModel.DoesNotExist:
        raise Http404
    except Exception as e:
        raise

def EliminarPersona(id):
    try:
        persona = PersonaModel.objects.filter(id=id)
        persona.delete()
    except PersonaModel.DoesNotExist:
        raise Http404
    except Exception as e:
        raise
        

"""Guarda una persona si la data en el serializador es correcta"""
def GuardarPersona(serializer):
    try:
        serializer.save()
    except Exception as e:
        raise

def UpdatePersona(id, data):
    try:
        persona = PersonaModel.objects.get(pk=id)
        serializer = PersonaSerializer(persona, data=data)
        if serializer.is_valid():
            serializer.save()
            return serializer.data
    except Exception as e:
        raise

