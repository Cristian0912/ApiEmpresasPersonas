from django.conf.urls import url
from api.v1.persona_component.views.persona_view import PersonaView, DetallePersonas

urlpatterns = [
    url(r'^$', PersonaView.as_view()),
    url(r'^(?P<pk>[\w\.-]+)/$', DetallePersonas.as_view()),
]