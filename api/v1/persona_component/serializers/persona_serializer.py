from rest_framework import serializers
from apps.persona.models import PersonaModel

class PersonaSerializer(serializers.ModelSerializer):
    class Meta():
        model = PersonaModel
        fields = ('nombres', 'apellidos', 'edad', 'direccion', 'correo', 'empresa_labora')
        read_only = ('id',)
        required = ('nombres', 'edad', 'correo', 'empresa_labora')