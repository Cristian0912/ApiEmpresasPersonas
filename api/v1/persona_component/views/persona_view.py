from api.v1.persona_component.controllers.persona_controller import PersonaController
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics, status
from rest_framework import permissions

class PersonaView(APIView):
    def get(self, request, format=None):
        try:
            data = PersonaController.getPersonas()
            return  Response(data)
        except Exception as e:
            return Response({'message': "Unexpected error occurred.....{}".format(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def post(self, request, format=None):
        try:
            persona = PersonaController.savePersona(request.data)
            if hasattr(persona, 'errors') and persona.errors:
                return Response(persona.errors, status=status.HTTP_400_BAD_REQUEST)
            return Response(persona.data, status=status.HTTP_201_CREATED)
        except Exception as e:
            return  Response(data={'message': "Unexpected error occurred : {}".format(e)},
                          status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class DetallePersonas(APIView):

    def get(self, request, pk):
        persona = PersonaController.getPersona(pk)
        return Response(persona)

    def delete(self, request, pk, format=None):
        try:
            persona = PersonaController.delpersona(pk)
            return Response(persona, status=status.HTTP_204_NO_CONTENT)
        except Exception as e:
            return Response(data={'message': "Unexpected error occurred : {}".format(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def put(self, request, pk, format=None):
        try:
            persona = PersonaController.updatePersona(pk, request.data)
            if hasattr(persona, 'errors') and persona.errors:
                return Response(persona.errors, status=status.HTTP_400_BAD_REQUEST)
            return Response(persona, status=status.HTTP_201_CREATED)
        except Exception as e:
            return Response(data={'message': "Unexpectsed error occurred : {}".format(e)},
                        status=status.HTTP_500_INTERNAL_SERVER_ERROR)

