# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-10 20:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='EmpresaModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('razon_social', models.CharField(default='', max_length=30)),
            ],
            options={
                'db_table': 'table_empresa',
            },
        ),
        migrations.CreateModel(
            name='PersonaModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombres', models.CharField(default='', max_length=50)),
                ('apellidos', models.CharField(default='', max_length=50)),
                ('edad', models.IntegerField(default=0)),
                ('direccion', models.CharField(default='', max_length=60)),
                ('correo', models.EmailField(default='', max_length=254)),
                ('empresa_labora', models.ManyToManyField(blank=True, to='persona.EmpresaModel')),
            ],
            options={
                'db_table': 'table_persona',
            },
        ),
    ]
