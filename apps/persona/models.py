from django.db import models
# Create your models here.

class EmpresaModel(models.Model):
    razon_social = models.CharField(max_length=30, default='')

    class Meta():
        db_table = 'table_empresa'

class PersonaModel(models.Model):

    id = models.IntegerField(primary_key=True)
    nombres = models.CharField(max_length=50, default='')
    apellidos = models.CharField(max_length=50, default='')
    edad = models.IntegerField(default=0)
    direccion = models.CharField(max_length=60, default='')
    correo = models.EmailField(default='')
    empresa_labora = models.ManyToManyField(EmpresaModel,blank=True)

    class Meta():
        db_table = 'table_persona'